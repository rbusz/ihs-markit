IHS Markit - recruitment task
-----------------------------
This project is a solution for a recruitment task at IHS Markit.
Suggested scala functions were changed to return BigDecimal type in order to preserve precision.

Technical Stack:
* Scala 2.12
* Gradle

Requirements:
* JDK8

Building the project:
* `./gradlew clean build`

**What could be done better?**
- Using asynchronous API for fetching charting data from Yahoo - it would allow to use these functions in Vert.x/Akka like environment
- Dockerise the build process - we could create a base Docker image which would have a set of Make tasks/bash scripts which could build and test our services/libraries. The resulting artifacts would be always identical, no matter what machine would be building it. In order to build such project a Software Developer would only require installed docker.
- Optimisation: http-client could be used with pooling and alive connections or even use HTTP 2 if Yahoo supports it, caching of prices etc.
- Configure and add logging