package eu.busz.ihs.markit.stock.prices

import java.time.{Clock, LocalDate, ZoneOffset}

import eu.busz.ihs.markit.stock.prices.yahoo.{YahooPriceClient, YahooStockPriceEntry, YahooStockPriceResource}
import org.junit.runner.RunWith
import org.mockito
import org.mockito.BDDMockito.given
import org.scalatest.junit.JUnitRunner
import org.scalatest.mockito.MockitoSugar.mock
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

@RunWith(classOf[JUnitRunner])
class HistoricStockPricesTest extends FunSuite with Matchers with BeforeAndAfter {

  val FixedClock = Clock.fixed(LocalDate.of(2017, 1, 1).atStartOfDay().toInstant(ZoneOffset.UTC), ZoneOffset.UTC)
  val Ticker = "ORL"
  val UnknownTicker = "GOO"

  var httpClientMock: YahooPriceClient = _
  var resource: StockPriceResource = _

  before {
    httpClientMock = mock[YahooPriceClient]
    resource = new YahooStockPriceResource(httpClientMock, FixedClock)
  }

  test("Given a ticker, return 1 year of historic prices") {
    val yahooPriceResult = List(YahooStockPriceEntry("", "10", "10", "10", "10", "10", "10"),
      YahooStockPriceEntry("", "10", "10", "10", "20", "10", "10"))
    val expectedPriceList = List[BigDecimal](10, 20)
    given(httpClientMock.fetchPrices(mockito.Matchers.anyObject())).willReturn(yahooPriceResult)

    resource.dailyPrices(Ticker) should equal (expectedPriceList)
  }

  test("Given an unknown ticker, return empty list of historic prices") {
    given(httpClientMock.fetchPrices(mockito.Matchers.anyObject())).willReturn(List[YahooStockPriceEntry]())
    resource.dailyPrices(UnknownTicker) should equal (List())
  }

  test("Given a ticker, returns zero price for corrupted price data input") {
    val corruptedPriceResult = List(YahooStockPriceEntry("", "10", "10", "10", "abca", "10", "10"),
      YahooStockPriceEntry("", "10", "10", "10", "NULL", "10", "10"),
      YahooStockPriceEntry("", "10", "10", "10", "", "10", "10"))
    given(httpClientMock.fetchPrices(mockito.Matchers.anyObject())).willReturn(corruptedPriceResult)

    resource.dailyPrices(UnknownTicker) should equal (List[Double](0,0,0))
  }

  test("Given a ticker, return 1 year daily returns") {
    val yahooPriceResult = List(YahooStockPriceEntry("", "10", "10", "10", "10", "10", "10"),
      YahooStockPriceEntry("", "10", "10", "10", "20", "10", "10"),
      YahooStockPriceEntry("", "10", "10", "10", "30", "10", "10"),
      YahooStockPriceEntry("", "10", "10", "10", "10", "10", "10"))
    given(httpClientMock.fetchPrices(mockito.Matchers.anyObject())).willReturn(yahooPriceResult)

    resource.dailyReturns(Ticker) should equal (List[BigDecimal](BigDecimal(1), BigDecimal(0.5), BigDecimal(10-30)/BigDecimal(30)))
  }

  test("Given a ticker, return 1 year mean return") {
    val yahooPriceResult = List(YahooStockPriceEntry("", "10", "10", "10", "10", "10", "10"),
      YahooStockPriceEntry("", "10", "10", "10", "20", "10", "10"),
      YahooStockPriceEntry("", "10", "10", "10", "30", "10", "10"),
      YahooStockPriceEntry("", "10", "10", "10", "10", "10", "10"))
    given(httpClientMock.fetchPrices(mockito.Matchers.anyObject())).willReturn(yahooPriceResult)

    resource.meanReturn(Ticker) should equal (BigDecimal(17.5))
  }
}
