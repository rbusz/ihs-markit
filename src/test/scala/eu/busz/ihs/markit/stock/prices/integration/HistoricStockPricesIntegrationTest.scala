package eu.busz.ihs.markit.stock.prices.integration

import java.time.{Clock, LocalDate, ZoneOffset}

import eu.busz.ihs.markit.stock.prices.StockPriceResource
import eu.busz.ihs.markit.stock.prices.yahoo.{YahooPriceClient, YahooStockPriceResource}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

@RunWith(classOf[JUnitRunner])
class HistoricStockPricesIntegrationTest extends FunSuite with Matchers with BeforeAndAfter {

  val GoogleTicker = "GOOGL"

  var httpClientMock: YahooPriceClient = _
  var resource: StockPriceResource = _

  before {
    val fixedClock = Clock.fixed(LocalDate.of(2017, 1, 1).atStartOfDay().toInstant(ZoneOffset.UTC), ZoneOffset.UTC)
      resource = new YahooStockPriceResource(new YahooPriceClient(), fixedClock)
  }

  test("Given Google's ticker return 1 year of historic prices from yahoo finance") {
    val prices = resource.dailyPrices(GoogleTicker)
    prices should not be empty
  }

}
