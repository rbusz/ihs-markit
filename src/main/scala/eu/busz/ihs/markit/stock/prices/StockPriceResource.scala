package eu.busz.ihs.markit.stock.prices

trait StockPriceResource {

  /**
    * @param ticker stock ticker symbol
    * @return 1 year mean return for ticker. Corrupted values are omitted
    */
  def meanReturn(ticker: String): BigDecimal

  /**
    *
    * @param ticker stock ticker symbol
    * @return 1 year daily returns for ticker. Corrupted values are substituted with '0'
    */
  def dailyReturns(ticker: String): List[BigDecimal]

  /**
    *
    * @param ticker stock ticker symbol
    * @return 1 year historic prices for ticker. Corrupted values are substituted with '0'
    */
  def dailyPrices(ticker: String): List[BigDecimal]

}
