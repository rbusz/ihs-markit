package eu.busz.ihs.markit.stock.prices.yahoo

import java.time.{Clock, LocalDate}

import eu.busz.ihs.markit.stock.prices.StockPriceResource

import scala.util.{Failure, Success, Try}

class YahooStockPriceResource(client: YahooPriceClient, clock: Clock) extends StockPriceResource {

  /**
    * {@inheritDoc}
    */
  override def dailyPrices(ticker: String): List[BigDecimal] = {
    client.fetchPrices(toRequest(ticker))
      .map(v => Try(BigDecimal(v.close)).getOrElse(BigDecimal(0)))
  }

  /**
    * {@inheritDoc}
    */
  override def dailyReturns(ticker: String): List[BigDecimal] = {
    val closePrices = client.fetchPrices(toRequest(ticker))
      .map(_.close)

    closePrices
      .zip(closePrices.drop(1))
      .map({ case (old, curr) =>
        (for {
          oldVal <- Try(BigDecimal(old))
          currVal <- Try(BigDecimal(curr))
          res <- Try((currVal - oldVal) / oldVal)
        } yield res).getOrElse(BigDecimal(0))
      })
  }

  /**
    * {@inheritDoc}
    */
  override def meanReturn(ticker: String): BigDecimal = {
    val (total, count) = client.fetchPrices(toRequest(ticker))
      .map(_.close)
      .filter(isDouble)
      .foldLeft(BigDecimal(0), BigDecimal(0)) {
        case ((acc, cnt), el: String) => (acc + BigDecimal(el), cnt + 1)
      }
    total / count
  }

  private def toRequest(ticker: String): YahooClientRequest = {
    val to = LocalDate.now(clock)
    val from = to.minusYears(1)
    YahooClientRequest(ticker, from, to)
  }

  private def isDouble(s: String): Boolean = {
    Try[Double](s.toDouble) match {
      case Success(_) => true
      case Failure(_) => false
    }
  }

}