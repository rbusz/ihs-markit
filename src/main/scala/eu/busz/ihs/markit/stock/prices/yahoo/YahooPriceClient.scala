package eu.busz.ihs.markit.stock.prices.yahoo

import java.time.LocalDate
import scala.io.Source

case class YahooClientRequest(tickerSymbol: String, dateFrom: LocalDate, dateTo: LocalDate)
case class YahooStockPriceEntry(date: String, open: String, high: String, low: String, close: String,
                                volume: String, adjClose: String)
class YahooPriceClient {

  /**
    * Fetch prices from Yahoo finance for given request object
    */
  def fetchPrices(request: YahooClientRequest): List[YahooStockPriceEntry] = {
    Source.fromURL(targetStockPriceUrl(request))
      .getLines()
      .drop(1)
      .map(_.split(YahooPriceClient.CsvSplitOperator))
      .map(arr => YahooStockPriceEntry(arr(0), arr(1), arr(2), arr(3), arr(4), arr(5), arr(6)))
      .toList
  }

  private def targetStockPriceUrl(request: YahooClientRequest): String = {
    val lastYear = request.dateFrom
    val today = request.dateTo

    f"http://real-chart.finance.yahoo.com/table.csv?s=${request.tickerSymbol}&a=${lastYear.getMonthValue}" +
      f"&b=${lastYear.getDayOfMonth}&c=${lastYear.getYear}&d=${today.getMonthValue}&e=${today.getDayOfMonth}&" +
      f"f=${today.getYear}&g=d&ignore=.csv"
  }

}

object YahooPriceClient {
  val CsvSplitOperator = ","
}